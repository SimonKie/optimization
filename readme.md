# Optimierung von Programmen

## Hinweise des Dozenten

Die Prüfungsleistung für diese Veranstaltung können Sie von zu Hause erbringen.
Was sie vor der  Bearbeitung des ersten Pflichtaufgabe tun sollten:

* `g++` installieren.
* Sich die beiden Vorlesungsfolien ansehen. Der Teil über Profiling ist für die
  Bearbeitung der Aufgaben nicht so wesentlich.
* Die beiden Übungsblätter bearbeiten (nicht die Pflichtaufgaben). Insbesondere
  den Raytracer übersetzen, ausführen usw.

Die Beschreibung der drei Pflichtaufgaben sollte eigentlich auch ausreichen. Im
Vorlesungsteil habe ich diese am Ende an der Tafel detaillierter erläutert.
Diese Unterlagen habe ich allerdings nur handschriftlich (und im Büro, in das
ich nicht mehr hinein sollte). Sobald die Einschränkungen zur
Präsenzveranstaltungen aufgehoben sind, kann ich diese Erläuterungen noch
nachholen. Als Vereinfachung in diesem Semester habe ich die spätmöglichste
Abgabe bis ganz zum Vorlesungsende geschoben.

**Bitte geben sie aber trotzdem Teilaufgaben vorher ab**, inklusive eines
Projektberichts über diesen Teil. Die zu messenden Zeiten müssen sie bis auf
weiteres auf Ihrem Rechner messen und nicht wie angegeben in E203.

## What is the difference between `g++` and `gcc`?

[Answer on StackOverflow](https://stackoverflow.com/questions/172587): gcc and
g++ are compiler-drivers of the GNU Compiler Collection (which was once upon a
time just the GNU C Compiler). Even though they automatically determine which
backends (cc1 cc1plus ...) to call depending on the file-type, unless overridden
with -x language, they have some differences. The probably most important
difference in their defaults is which libraries they link against automatically.

## Creating the Lab Report

With `pdflatex` installed, run

```bash
cd docs
pdflatex lab-report.tex
```

to create the lab report PDF.

## Creating annotated assembler code

```bash
g++ -Wall -pedantic -c -g -O3 -mavx2 -march=native -mfpmath=sse example.cc
objdump -S example.o > example.s
```
