#include "kdtree.h"

BoundingBox::BoundingBox() { }

BoundingBox::BoundingBox(Vector<FLOAT,3> min, Vector<FLOAT,3> max) 
 : min(min), max(max) { }

void BoundingBox::split(BoundingBox & left, BoundingBox & right) {
  // the box is split along the longest axis, so we calculate:
  FLOAT lengthX = fabs(max[0] - min[0]);
  FLOAT lengthY = fabs(max[1] - min[1]);
  FLOAT lengthZ = fabs(max[2] - min[2]);

  // depending on which axis is the longest, we cut the area in half
  if(lengthX >= lengthY && lengthX >= lengthZ) {
    // the x-axis is the longest, so we split there
    left.min = Vector<FLOAT, 3>({min[0], min[1], min[2]});
    left.max = Vector<FLOAT, 3>({min[0] + (lengthX / 2), max[1], max[2]});
    right.min = Vector<FLOAT, 3>({min[0] + (lengthX / 2), min[1], min[2]});
    right.max = Vector<FLOAT, 3>({max[0], max[1], max[2]});
  } else if (lengthY >= lengthX && lengthY >= lengthZ) {
    // the y-axis is the longest, so we split there
    left.min = Vector<FLOAT, 3>({min[0], min[1], min[2]});
    left.max = Vector<FLOAT, 3>({max[0], min[1] + (lengthY / 2), max[2]});
    right.min = Vector<FLOAT, 3>({min[0], min[1] + (lengthY / 2), min[2]});
    right.max = Vector<FLOAT, 3>({max[0], max[1], max[2]});
  } else if (lengthZ >= lengthX && lengthZ >= lengthY) {
     // the z-axis is the longest, so we split there
    left.min = Vector<FLOAT, 3>({min[0], min[1], min[2]});
    left.max = Vector<FLOAT, 3>({max[0], max[1], min[2] + (lengthZ / 2)});
    right.min = Vector<FLOAT, 3>({min[0], min[1], min[2] + (lengthZ / 2)});
    right.max = Vector<FLOAT, 3>({max[0], max[1], max[2]});
  }
}

bool BoundingBox::contains(Vector<FLOAT, 3> v) {
  // check if x, y and z are in between min and max
  return (v[0] >= min[0] && v[0] <= max[0]) &&
         (v[1] >= min[1] && v[1] <= max[1]) &&
         (v[2] >= min[2] && v[2] <= max[2]);
}

bool BoundingBox::contains(Triangle<FLOAT> *triangle) {
  // a boundingBox contains a triangle if at least one of the three
  // corner points is contained in the boundingBox, so we check for
  // each of them
  return contains(triangle->p1) ||
         contains(triangle->p2) ||
         contains(triangle->p3);
}

bool BoundingBox::intersects(Vector<FLOAT,3> eye, Vector<FLOAT, 3> direction) {
    // slab test implementation
    FLOAT tmin[3] = { (min[0] - eye[0]) / direction[0],
                      (min[1] - eye[1]) / direction[1],
                      (min[2] - eye[2]) / direction[2] };
    FLOAT tmax[3] = { (max[0] - eye[0]) / direction[0],
                      (max[1] - eye[1]) / direction[1],
                      (max[2] - eye[2]) / direction[2] };
    FLOAT tminimum = std::min(tmin[0], tmax[0]);
    FLOAT tmaximum = std::max(tmin[0], tmax[0]);
    tminimum = std::max(tminimum, std::min(tmin[1], tmax[1]) );
    tmaximum = std::min(tmaximum, std::max(tmin[1], tmax[1]) );
    tminimum = std::max(tminimum, std::min(tmin[2], tmax[2]) );
    tmaximum = std::min(tmaximum, std::max(tmin[2], tmax[2]) );

    return tmaximum >= tminimum;
}


KDTree::~KDTree() {
  delete left;
  delete right;
}

KDTree * KDTree::buildTree(KDTree * tree, std::vector< Triangle<FLOAT> *> & triangles) {
  // if there are less than MAX_TRIANGLES_PER_LEAF triangles, we don't need to
  // split the tree any further
  if(MAX_TRIANGLES_PER_LEAF >= triangles.size()) {
    tree->triangles.insert(tree->triangles.end(), triangles.begin(), triangles.end());
    return nullptr;
  }

  // this means there are more than MAX_TRIANGLES_PER_LEAF triangles, so we
  // create children nodes
  tree->left = new KDTree();
  tree->right = new KDTree();

  // we split the bounding box of our current tree node in half, assigning them
  // to the left and right child node
  tree->box.split(tree->left->box, tree->right->box);

  // we create containers for our soon-to-be separated triangles
  std::vector<Triangle<FLOAT> *> trianglesLeft;
  std::vector<Triangle<FLOAT> *> trianglesRight;

  // now we need to check where each triangle is supposed to go
  for(std::vector<Triangle<FLOAT> *>::iterator t = triangles.begin(); t != triangles.end(); ++t) {
    // we check if the bounding boxes contain the triangle
    bool isInLeftBox = tree->left->box.contains(*t);
    bool isInRightBox = tree->right->box.contains(*t);
    if(isInLeftBox && isInRightBox) {
      // this triangle stays with the current node
      tree->triangles.push_back(*t);
    } else if (isInLeftBox) {
      // this triangle belongs to the left child
      trianglesLeft.push_back(*t);
    } else if (isInRightBox) {
      // this triangle belongs to the right child
      trianglesRight.push_back(*t);
    }
    
  }

  // building the subtrees recursively
  tree->buildTree(tree->left, trianglesLeft);
  tree->buildTree(tree->right, trianglesRight);


  return tree;
}

KDTree *  KDTree::buildTree(std::vector< Triangle<FLOAT> *> & triangles)  {
  // this function is being called by the raytracer before the actual
  // raytracing, handing over all the triangles from the object

  // We create a new Tree as our root node
  KDTree * root = new KDTree();

  // we initialize the bounding box with infinity values to minimize them
  // during the next step

  FLOAT inf = std::numeric_limits<FLOAT>::infinity();

  Vector<FLOAT, 3> boundingBoxMin = { inf, inf, inf };
  Vector<FLOAT, 3> boundingBoxMax = { -inf, -inf, -inf };

  // We calculate the minimum and maximum vector of all the triangles
  // in order to calculate our all-surrounding bounding box
  for (auto const &triangle : triangles)
  {
    boundingBoxMin[0] = std::min(std::min(std::min(boundingBoxMin[0], triangle->p1[0]), triangle->p2[0]), triangle->p3[0]);
    boundingBoxMin[1] = std::min(std::min(std::min(boundingBoxMin[1], triangle->p1[1]), triangle->p2[1]), triangle->p3[1]);
    boundingBoxMin[2] = std::min(std::min(std::min(boundingBoxMin[2], triangle->p1[2]), triangle->p2[2]), triangle->p3[2]);

    boundingBoxMax[0] = std::max(std::max(std::max(boundingBoxMax[0], triangle->p1[0]), triangle->p2[0]), triangle->p3[0]);
    boundingBoxMax[1] = std::max(std::max(std::max(boundingBoxMax[1], triangle->p1[1]), triangle->p2[1]), triangle->p3[1]);
    boundingBoxMax[2] = std::max(std::max(std::max(boundingBoxMax[2], triangle->p1[2]), triangle->p2[2]), triangle->p3[2]);
  }

  // Some YAML formatted output
  std::cerr << "kdtree:" << std::endl;
  std::cerr << "  min:" << std::endl;
  std::cerr << "    x: " << boundingBoxMin[0] << std::endl;
  std::cerr << "    y: " << boundingBoxMin[1] << std::endl;
  std::cerr << "    z: " << boundingBoxMin[2] << std::endl;
  std::cerr << "  max:" << std::endl;
  std::cerr << "    x: " << boundingBoxMax[0] << std::endl;
  std::cerr << "    y: " << boundingBoxMax[1] << std::endl;
  std::cerr << "    z: " << boundingBoxMax[2] << std::endl;

  // We construct the bounding box with the paramaters we just calculated
  root->box = BoundingBox(boundingBoxMin, boundingBoxMax);

  // Then we call the buildTree() function on the root node, which starts the
  // process of recursive splitting
  root->buildTree(root, triangles);

  // we then return the root node

  return root;
}

bool KDTree::hasNearestTriangle(Vector<FLOAT,3> eye, Vector<FLOAT,3> direction, Triangle<FLOAT> *  & nearest_triangle, FLOAT &t, FLOAT &u, FLOAT &v, FLOAT minimum_t) {
  // if the ray doesn't intersect with the bounding box, we can ignore it
  if(!box.intersects(eye, direction)) {
    return false;
  }

  // Execute test for each subtree if a subtree exists
  if (left != nullptr && left->hasNearestTriangle(eye, direction, nearest_triangle, t, u, v, minimum_t)) {
    minimum_t = t;
  }
  if (right != nullptr && right->hasNearestTriangle(eye, direction, nearest_triangle, t, u, v, minimum_t)) {
    minimum_t = t;
  }

  // Check each of the triangles for the nearest intersection
  for(std::vector<Triangle<FLOAT> *>::iterator tr = triangles.begin(); tr != triangles.end(); ++tr) {
    // if we actually check, we need to increase the counter
    stats.no_ray_triangle_intersection_tests++;

    // Check if one of the triangles intersects
    if ((*tr)->intersects(eye, direction, t, u, v, minimum_t))
    {
      // This means we have a hit, so we increase
      // the counter for "intersections found"
      stats.no_ray_triangle_intersections_found++;
      if (nearest_triangle == nullptr || t < minimum_t)
      {
        minimum_t = t;
        nearest_triangle = (*tr);
      }
    }
  }

  t = minimum_t;

  return nearest_triangle != nullptr;
}
