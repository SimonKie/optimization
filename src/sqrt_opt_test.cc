// g++ -Wall -pedantic -march=native -mfpmath=sse -msse -O3 sqrt_opt.cc
// für objdump:
// g++ -Wall -pedantic -march=native -mfpmath=sse -msse -O3 sqrt_opt.cc -g -c
// objdump -S sqrt_sse.o 
#include <iostream>
#include <limits>
#include <random>
#include "measure_time.h"
#include "sqrt_opt.h"

double random_double()
{
  static std::random_device device;
  static std::uniform_real_distribution<double> dist(0.0, 10000.0);
  return dist(device);
}

template <size_t LOOPS = 2>
void test_sqrt(void) {
    const static int LOOP = 1;
    const static int N = 2;

    std::cout << "======== "<< LOOPS  << " ITERATIONS ========" << std::endl;

    alignas(128) float floats[8] = {0, 0.34, 16, 23, 128, 1024, 45345, 123456};
    alignas(128) float roots[8];
    alignas(128) float benchmark[8];
    int eightcounter = 0;

    std::cout << "\nmath sqrt" << std::endl;

    for (int j = 0; j < LOOP; j++) {
      for (int i = 0; i < 4 * N; i += 4) {
        for (int k = 0; k < 4; k++) {
          roots[i + k] = sqrt(floats[i + k]);
          benchmark[eightcounter++] = roots[i + k];
          std::cout << "- For " << floats[i + k] << " calculated root " << roots[i + k] << std::endl;
        }
      }    
    }
    
    std::cout << "\nsqrt1 (Newton method for single float, one time a loop)" << std::endl;
    eightcounter = 0;
    
    for (int j = 0; j < LOOP; j++) {
      for (int i = 0; i < 4 * N; i++) {
        roots[i] = sqrt1<LOOPS>(floats + i);
        std::cout << "- For " << *(floats + i) << ", result off by " << (benchmark[eightcounter++] - roots[i]) << std::endl;
      }
    }    

    std::cout << "\nsqrt1 (Newton method for single float, four times a loop)" << std::endl;
    eightcounter = 0;
    
    for (int j = 0; j < LOOP; j++) {
      for (int i = 0; i < 4 * N; i += 4) {
        for (int k = 0; k < 4; k++) {
          roots[i + k] = sqrt1<LOOPS>(floats + i + k);
          std::cout << "- For " << *(floats + i + k) << ", result off by " << (benchmark[eightcounter++] - roots[i + k]) << std::endl;
        }
      }
    }   

    std::cout << "\nsqrt2 (Newton method for sequence of 4 floats)" << std::endl;
    eightcounter = 0;

    for (int j = 0; j < LOOP; j++) {
      for (int i = 0; i < 4 * N; i += 4) {
        sqrt2<LOOPS>(floats + i, roots + i);
        std::cout << "- For " << *(floats + i) << ", result off by " << (benchmark[eightcounter++] - *(roots + i)) << std::endl;
        std::cout << "- For " << *(floats + i + 1) << ", result off by " << (benchmark[eightcounter++] - *(roots + i + 1)) << std::endl;
        std::cout << "- For " << *(floats + i + 2) << ", result off by " << (benchmark[eightcounter++] - *(roots + i + 2)) << std::endl;
        std::cout << "- For " << *(floats + i + 3) << ", result off by " << (benchmark[eightcounter++] - *(roots + i + 3)) << std::endl;
      }
    }

    std::cout << "\nsqrt3 (Newton method for sequence of 4 floats, SIMD)" << std::endl;
    eightcounter = 0;

    for (int j = 0; j < LOOP; j++) {
      for (int i = 0; i < 4 * N; i += 4) {
        sqrt3<LOOPS>(floats + i, roots + i);
        std::cout << "- For " << *(floats + i) << ", result off by " << (benchmark[eightcounter++] - *(roots + i)) << std::endl;
        std::cout << "- For " << *(floats + i + 1) << ", result off by " << (benchmark[eightcounter++] - *(roots + i + 1)) << std::endl;
        std::cout << "- For " << *(floats + i + 2) << ", result off by " << (benchmark[eightcounter++] - *(roots + i + 2)) << std::endl;
        std::cout << "- For " << *(floats + i + 3) << ", result off by " << (benchmark[eightcounter++] - *(roots + i + 3)) << std::endl;
      }
    }  

    std::cout << "\n\n" << std::endl;
}

int main(void) {
    test_sqrt<2>();
    test_sqrt<3>();
    test_sqrt<4>();
    return 1;
}

