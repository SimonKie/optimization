# Profiling exercise

```bash
# Compile taylor.cc for profiling
g++ -Wall -pedantic -fprofile-arcs -ftest-coverage taylor.cc
./a.out
gcov taylor.cc

# Compile taylor.cc for analysis with gprof
g++ -Wall -pedantic -pg taylor.cc
./a.out
gprof > taylor.gprof
```

Results for original

```txt
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  us/call  us/call  name
 50.15      0.02     0.02    10001     2.01     4.01  e_hoch_x(double, unsigned long)
 25.07      0.03     0.01  9990999     0.00     0.00  power(double, unsigned long)
 25.07      0.04     0.01  9990999     0.00     0.00  faculty(double)
  0.00      0.04     0.00        1     0.00     0.00  _GLOBAL__sub_I__Z5powerdm
  0.00      0.04     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
```

Results for optimized version

```txt
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  us/call  us/call  name
100.29      0.09     0.09    10001     9.03     9.03  e_hoch_x_1(double, unsigned long)
  0.00      0.09     0.00        1     0.00     0.00  _GLOBAL__sub_I__Z5powerdm
  0.00      0.09     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
```

It seems like the optimized version performs slower in the `gprof` profile,
however it runs much faster when executed directly on the command line.

## Profiling the Raytracer

```bash
# (in the src directory)

# Compiling the raytracer for analysis
g++ -Wall -pedantic -fprofile-arcs -ftest-coverage -O3 raytracer.cc statistics.cc

# Execute once for gcov
./a.out --no_ppm

# Execute gcov
gcov raytracer.cc

# Compiling the raytracer for profiling
g++ -Wall -pedantic -pg -O3 raytracer.cc statistics.cc

# Create profile
gprof > raytracer.gprof
```

Results:

* The for loop in the function `hasNearestTriangle` is the most-executed line in
  the raytracer.
* Also high: `getOrigin` and `getDirection`

Flat profile of `gprof`:

```txt
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  Ts/call  Ts/call  name
100.02      9.96     9.96                             raytrace(Camera&, Scene&, Screen&, KDTree*)
  0.00      9.96     0.00       14     0.00     0.00  void std::vector<Triangle<float>, std::allocator<Triangle<float> > >::_M_realloc_insert<Triangle<float> const&>(__gnu_cxx::__normal_iterator<Triangle<float>*, std::vector<Triangle<float>, std::allocator<Triangle<float> > > >, Triangle<float> const&)
  0.00      9.96     0.00       13     0.00     0.00  void std::vector<Vector<float, 3ul>, std::allocator<Vector<float, 3ul> > >::_M_realloc_insert<Vector<float, 3ul> const&>(__gnu_cxx::__normal_iterator<Vector<float, 3ul>*, std::vector<Vector<float, 3ul>, std::allocator<Vector<float, 3ul> > > >, Vector<float, 3ul> const&)
  0.00      9.96     0.00        1     0.00     0.00  _GLOBAL__sub_I__ZN10Statistics5printEv
  0.00      9.96     0.00        1     0.00     0.00  _GLOBAL__sub_I__ZlsRSoRK6Screen
```
