# C++ Exercise, 21.03.2020

## Intro

```bash
# in directory src
g++ -Wall -pedantic -O3 raytracer.cc statistics.cc
./a.out --no_ppm
```

Output:

```txt
reading from       : examples/teapot.obj
writing bmp to     : output.bmp
resolution x set to: 256
resolution y set to: 256
vertices reversed  : 0
write ppm          : 0
no of triangles : 6320
no of vertices : 3644
no of normals : 3644
memory used for all triangles [byte] : 455040
Statistics
  FLOAT = float, sizeof(FLOAT) = 4
  Raytracing time: 10.1565 [s] .
  Number of ray / triangle intersection tests : 519953565
  Number of ray / triangle intersections found : 38217
```

To display the teapot:

```bash
./a.out | display
```

Increasing the resolution by factor 2 on both axes leads to a runtime that is
approximately 4 times as long.

```bash
./a.out --width 512 --height 512 | display
```

Result:

```txt
reading from       : examples/teapot.obj
writing bmp to     : output.bmp
resolution x set to: 512
resolution y set to: 512
vertices reversed  : 0
write ppm          : 1
no of triangles : 6320
no of vertices : 3644
no of normals : 3644
memory used for all triangles [byte] : 455040
Statistics
  FLOAT = float, sizeof(FLOAT) = 4
  Raytracing time: 40.8159 [s] .
  Number of ray / triangle intersection tests : 2079878225
  Number of ray / triangle intersections found : 153039
```

## G++ Compiler

Directory `src/0-intro`

```bash
# No errors when compiling like this
g++ example.cc

# No output
./a.out

# Pedantic compiling, throws warning but compiles anyway
g++ -Wall -pedantic example.cc

# Create platform-independent object files with flag -c
g++ -Wall -pedantic -c example.cc

# Only pass object file to compiler to execute linker
g++ example.o

# Specify a name for output file
g++ example.o -o example

# Compile assembly code
g++ -Wall -pedantic -S example.cc

# Compile with debug information
g++ -Wall -pedantic -S -g example.cc

# Create object file with debug information and use
# objdump to convert it to annotated assembly code
g++ -Wall -pedantic -c -g example.cc
objdump -S example.o > example.s

# Using the highest optimization level and recreate assembly file
g++ -Wall -pedantic -c -g -O3 example.cc
objdump -S example.o > example.s

# Replacing literal float 2.0 with variable, then:
g++ -Wall -pedantic -c -g -O3 -D ZAHL=4.0 example.cc
objdump -S example.o > example.s

# Additional optimization based on X86, if possible
g++ -Wall -pedantic -c -g -O3 -mavx2 -march=native -mfpmath=sse example.cc
objdump -S example.o > example.s
```

## C++ Basics

* Mit wie vielen Bits wird der Datentyp int codiert?
  * 16
* Was ist der Datentyp long double?
  * extended precision floating point type, 80 bits
* Was ist der Datentyp size_t? Wofür benötigt man ihn?
  * unsinged integer type of the sizeof operator to dtermine the size of objects
    or types.
* Welchen Wertebereich hat der Datentyp bool?
  * true and false.
* Wie sollte man einen Null-Pointer in C++11 angeben?
  * e. g. `int  *ptr = NULL;`. By convention, if a pointer contains the null
    (zero) value, it is assumed to point to nothing.
* Wie wird eine Referenz auf eine Variable deklariert? Wie wird diese verwendet?
  * With an `&` like so `string name = “Priya”; string & refName = name;`. A
    reference can be said as an alias to an existing variable. The main use of
    this variable is working as a parameter which works as a pass-by-reference.
    This is passed to a function. The function to which this is passed works on
    the original variable instead of the copy in a pass by value. The changes
    made inside the function will also be reflected outside.
* Was ist der Unterschied zwischen einem C-String und einen C++ std::string?
  * std::string uses C-strings under the hood, but makes it easier to deal with
    them. It hides the ugly details from you (like resizing the array that
    contains the c-string, comparison, concatenation, et cetera).
* Was ist der Unterschied zwischen einem struct und class in C++?
  * Technical: The only difference is if you don’t specify the visibility
    (public, private or protected) of the members, they will be public in the
    struct and private in the class.
  * Semantic: In a word, a struct is a bundle. A struct is several related
    elements that needed to be tied up together in a certain context. In two
    words, a class can do things. A class has responsibilities. You want to use
    the term class when you are modelling a concept (that has an existence in
    the business domain or not),the concept of a object that can perform
    actions.Contrary to a struct, a class is made to offer an interface, that
    has some degree of separation from its implementation.
  * Finally a simple rule of thumb for choosing between struct or class is to go
    for class whenever there is at least one private member in the structure.
    Indeed, this suggests that there are implementation details that are to be
    hidden by an interface, which is the purpose of a class.
* Was versteht man unter dem Überladen von Operatoren? Können neue Operatoren
  definiert, die Klammerungsregeln oder Operatorenbindung geändert werden?
  * You can overload operators like `+` in C++ to ensure special behavior that
    fits the object you're using the operator on:

    ```cplusplus
    #include<iostream>
    using namespace std;

    class Complex {
    private:
        int real, imag;
    public:
        Complex(int r = 0, int i =0)  {real = r;   imag = i;}

        // This is automatically called when '+' is used with
        // between two Complex objects
        Complex operator + (Complex const &obj) {
             Complex res;
             res.real = real + obj.real;
             res.imag = imag + obj.imag;
             return res;
        }
        void print() { cout << real << " + i" << imag << endl; }
    };

    int main()
    {
        Complex c1(10, 5), c2(2, 4);
        Complex c3 = c1 + c2; // An example call to "operator+"
        c3.print();
    }
    ```

  * New operators can't be defined.
* Der Cast-Operator aus C sollten in C++ nicht verwendet werden. Welche
  Cast-Operatoren stellt C++ zur Verfügung?
  * const_cast&gt;zieldatentyp&lt;(ausdruck)
  * static_cast&gt;zieldatentyp&lt;(ausdruck)
  * reinterpret_cast&gt;zieldatentyp&lt;(ausdruck)
  * dynamic_cast&gt;zieldatentyp&lt;(ausdruck)
  * The classic C cast can be dangerous. See sources for more information.
* Was ist eine Template-Funktion? Wie behandelt der Compiler Templates?
  * Ein Funktions-Template (auch fälschlich Template-Funktion genannt) verhält
    sich wie eine Funktion, die Argumente verschiedener Typen akzeptiert oder
    unterschiedliche Rückgabetypen liefert. Die C++-Standardbibliothek enthält
    beispielsweise das Funktions-Template `std::max(x, y)`, welches das größere
    der beiden Argumente zurückgibt.
  * Das Funktions-Template max() lässt sich für jeden Typ instanziieren, für den
    der Vergleich x < y eine wohldefinierte Operation darstellt. Bei
    selbstdefinierten Typen macht man von Operator-Überladung Gebrauch.
* Die C-Funktionen zur Ein- und Ausgabe von Daten sollten nicht verwendet
  werden. Wie funktioniert die Ein- und Ausgabe in C++? Warum sollten diese
  verwendet werden?
  * You use `cin` and `cout`, the latter with the insertion operator `<<`.
  * [comparison between printf and cout](https://stackoverflow.com/questions/2872543)
* Was versteht man unter nicht definiertem Verhalten? Nennen Sie Beispiele.
  * Unlike some programming languages, C/C++ does not initialize most variables
    to a given value (such as zero) automatically. Thus when a variable is
    assigned a memory location by the compiler, the default value of that
    variable is whatever (garbage) value happens to already be in that memory
    location! A variable that has not been given a known value (usually through
    initialization or assignment) is called an uninitialized variable.
  * Using the value from an uninitialized variable is our first example of
    undefined behavior. Undefined behavior is the result of executing code whose
    behavior is not well defined by the C++ language. In this case, the C++
    language doesn’t have any rules determining what happens if you use the
    value of a variable that has not been given a known value. Consequently, if
    you actually do this, undefined behavior will result.
* **Was versteht man unter plattformabhängigem Verhalten? Nennen Sie Beispiele.**
  * See [this answer on stackoverflow](https://stackoverflow.com/questions/11810484)
* **Wie ist dieses plattformabhängige Verhalten in der GCC und unter 64-Bit-Windows definiert. Wo muss man nachschlagen, um das herauszufinden?**
* **Erklären Sie nicht definiertes und plattformunabhängiges Verhalten am Beispiel des Rechtsschift-Operators.**
* Was ist der Call-stack?
  * Jede Funktion in C und C++ representiert ein kleines Unterprogramm, in
    welchem der entsprechende Code in einer abgeschlossenen und nach aussen
    nicht sichtbaren Umgebung ausgeführt wird. Um eine abgeschlossene Umgebung
    sicherzustellen, wird bei einem Funktionsaufruf ein Datenblock angelegt,
    welcher sämtliche benötigte Daten des aktuellen Programmablaufes speichert.
    Sobald die Funktion beendet ist, wird der Datenblock wieder abgebaut. Da
    Funktionsaufrufe sehr häufig und zudem ineinander verschachtelt auftreten,
    müssen diese Datenblöcke in einer einfach zu erweiternden, hierarchischen
    Ordnung gespeichert werden. In C und C++ werden Funktionen somit auf einem
    Stack gespeichert. Da dieser Stack für Funktionsaufrufe benutzt wird, wird
    er Call-Stack genannt.
* Was ist der Heap?
  * Der grösste Teil des Speicherplatzes ist heutzutage derjenige für die
    dynamischen Daten. Er wird Heap (zu Deutsch: Halde) genannt und steht dem
    Prozess grundsätzlich (mit einigen Restriktionen) zur freien Verfügung. Der
    Heap bezeichnet den insgesamt einem Prozess zur Verfügung stehenden
    Speicher. Auf modernen 32-Bit-Systemen können von jedem einzelnen Prozess
    bis zu 4 Gigabyte Speicher angesprochen werden, auf 64-Bit-Systemen sogar um
    ein Milliardenfaches mehr.
* Was ist der statische Speicherbereich?
  * C++ unterteilt den verfügbaren Speicher in vier Bereiche. Diese sind der
    * Programmspeicher,
    * globale Speicher für globale Variable,
    * Haldenspeicher (=> Heap) für die dynamische Speicherverwaltung, und
    * Stapelspeicher (statische Speicherverwaltung)
  * Für den Stapelspeicher gilt immer: Was zuletzt angefordert wurde, muss
    auch als erstes wieder freigegeben werden (LIFO: Last In – First Out).
    Wenn Sie innerhalb eines Blocks {;;;} also Variablen anlegen, werden diese
    auf dem Stack angelegt. Am Ende des Blocks verliert die Variable ihre
    Gültigkeit und der Speicher wird wieder freigegeben.
* Die Funktionen zur dynamischen Speicherverwaltung aus C sollten in C++ nicht
  verwendet werden. Wie erzeugt man in C++ dynamisch Speicher für einzelne Werte
  wie ein int und gibt ihn wieder frei?
  * C uses malloc() and calloc() function to allocate memory dynamically at run
    time and uses free() function to free dynamically allocated memory. C++
    supports these functions and also has two operators new and delete that
    perform the task of allocating and freeing the memory in a better and easier
    way.
* Wie werden Felder dynamisch angelegt und wieder freigegeben. Wie Objekte?
  * Angefordert wird Speicher in C++ über den Operator `new`. Im Gegensatz zur
    C-Variante wird in C++ immer Speicher für einen bestimmten Datentyp
    angefordert. Dieser darf allerdings nicht const oder volatile und auch keine
    Funktion sein. Der new-Operator erhält als Argument also einen Datentyp,
    anhand dessen er die Menge des benötigten Speichers selbstständig bestimmt.
    An den Konstruktor des Datentyps können, wenn nötig, Argumente übergeben
    werden, denn new gibt nicht nur einen Speicherbereich zurück, sondern legt
    in diesem Speicherbereich auch gleich noch ein Objekt des übergebenen Typs
    an. Zu beachten ist hierbei, dass für einen Aufruf ohne zusätzliche
    Konstruktorargumente keine leeren Klammern angegeben werden dürfen.
  * new gibt bei erfolgreicher Durchführung einen Zeiger auf den entsprechenden
    Datentyp zurück. Diesen Zeiger müssen Sie dann (beispielsweise in einer
    Variable) speichern. Wenn Sie das Objekt nicht mehr benötigen, müssen Sie es
    mittels delete-Operator wieder freigeben. delete erwartet zum Löschen den
    Zeiger, den new Ihnen geliefert hat. Analog zum Konstruktoraufruf bei new
    ruft delete zunächst den Destruktor des Objekts auf und gibt anschließend
    den Speicher an das Betriebssystem zurück.
* C++ kennt keine automatische Speicherverwaltung. Wie kann man in C++11
  sicherstellen, dass dynamisch angeforderter Speicher, der nicht mehr benötigt
  wird, garantiert immer freigegeben wird?
  * In C++, delete operator should only be used either for the pointers pointing
    to the memory allocated using new operator or for a NULL pointer, and free()
    should only be used either for the pointers pointing to the memory allocated
    using malloc() or for a NULL pointer.
* Was bedeutet Call-by-reference, call-by-value? Was sind die Unterschiede? In
  welchen Situationen wirken sich diese Unterschiede aus?
  * Call By Value: In this parameter passing method, values of actual parameters
    are copied to function’s formal parameters and the two types of parameters
    are stored in different memory locations. So any changes made inside
    functions are not reflected in actual parameters of caller.
  * Call by Reference: Both the actual and formal parameters refer to same
    locations, so any changes made inside the function are actually reflected in
    actual parameters of caller.
  * [Details](https://www.geeksforgeeks.org/difference-between-call-by-value-and-call-by-reference/)
* Was bedeutet die Dreier-Regel?
  * Die Dreierregel bezeichnet in der Programmiersprache C++ eine Faustregel,
    die empfiehlt, dass in einer Klasse, die eine der folgenden drei
    Elementfunktionen definiert, auch die jeweils anderen beiden definiert
    werden sollten:
    * Kopierkonstruktor
    * Destruktor
    * Zuweisungsoperator
  * [Details](https://de.m.wikipedia.org/wiki/Dreierregel_(C%2B%2B))
* Was (ab C++14) bedeutet die Fünfer-Regel?
  * Analog zur Dreier-Regel, plus zusätzlich:
    * Verschiebekonstruktor
    * Verschiebezuweisung
  * [Details](https://de.m.wikipedia.org/wiki/Dreierregel_(C%2B%2B)#F%C3%BCnferregel)

### Sources

* [C++ type reference](https://en.cppreference.com/w/cpp/language/types)
* [General concepts (in German)](https://manderc.com/index.php)
* [Reference vs Pointers](https://www.educba.com/c-plus-plus-reference-vs-pointer/)
* [std::string](http://www.cplusplus.com/reference/string/string/)
* [Struct vs Class](https://www.fluentcpp.com/2017/06/13/the-real-difference-between-struct-class/)
* [Operator Overloading](https://www.geeksforgeeks.org/operator-overloading-c/)
* [Casting in C++](https://www.straub.as/c-cpp-qt-fltk/cpp/casting.html)
* [Templates in C++](https://de.m.wikipedia.org/wiki/Template_(C%2B%2B))
* [Uninitialized variables and behaviors](https://www.learncpp.com/cpp-tutorial/uninitialized-variables-and-undefined-behavior/)
* [The Call Stack](https://manderc.com/concepts/callstack/index.php)
* [Speicherverwaltung](https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Speicherverwaltung)
* [New and Delet Operators](https://www.geeksforgeeks.org/new-and-delete-operators-in-cpp-for-dynamic-memory/)
