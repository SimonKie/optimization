#!/bin/bash

ITERATIONS=5
COMPILE=true
#COMPILE_COMMAND="g++ -Wall -pedantic -march=native -mfpmath=sse -mavx -O3 raytracer.cc statistics.cc -D OPTIMIZED_INTERSECTS"
#COMPILE_COMMAND="g++ -Wall -pedantic -march=native -mfpmath=sse -mavx2 -O3 sqrt_opt.cc"
#COMPILE_COMMAND="g++ -Wall -pedantic -std=c++11 -march=native -mfpmath=sse -msse -O3 raytracer.cc statistics.cc kdtree.cc"
COMPILE_COMMAND="g++ -Wall -pedantic -std=c++11 -march=native -mfpmath=sse -msse -O3  -D OPTIMIZED_INTERSECTS -D USE_KDTREE raytracer.cc statistics.cc kdtree.cc"

function usage()
{
    echo "Test the raytracer repeatedly."
    echo ""
    echo "./test.sh"
    echo "    -h --help"
    echo "    -i --iterations=$ITERATIONS"
    echo "    -n --nocompile"
    #echo "\t-f --flag=$VAR"
    echo ""
}

function compile()
{
    if [ $COMPILE = true ]
    then
        echo "Executing compile command:"
        echo $COMPILE_COMMAND
        eval $COMPILE_COMMAND
        echo "Done compiling"
    fi
}

function runTests() {
    counter=1
    fulltime=0

    # empty the logfile before execution
    echo "---" > ./../log.yaml

    while [ $counter -le $ITERATIONS ]
    do
        # redirect the raytracers error output to standard out,
        # then pipe it through yq (YAML handler) into a log file. 
        ./a.out --no_ppm 2>&1 | yq r - >> ./../log.yaml
        echo "---" >> ./../log.yaml

        # Use yq to get the time for the run
        #time=$(yq r ./../log.yaml -d "$((counter - 1))" "statistics.time")
        #echo "Iteration $counter: $time seconds"
        
        # Add the increment to the overall time
        #fulltime=$(python -c "print $fulltime + $time")
        
        ((counter++))
    done
}

function displayResults()
{
    # Calculate and display average time
    #avgtime=$(python -c "print $fulltime / $ITERATIONS")
    #echo "Average time for $ITERATIONS iterations: $avgtime seconds"
    echo "No results"
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        -i | --iterations)
            ITERATIONS=$VALUE
            ;;
        -c | --command)
            COMPILE_COMMAND=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

cd src
compile
runTests
displayResults
exit 0