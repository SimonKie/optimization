#!/bin/bash

ITERATIONS=10

function runTests() {
    counter=1
    iter=4
    sqrt_avg=0
    sqrt11_avg=0
    sqrt14_avg=0
    sqrt2_avg=0
    sqrt3_avg=0

    while [ $counter -le $ITERATIONS ]
    do
        # Use yq to get the time for the run
        sqrt=$(yq r ./results_lab2.yaml -d "$((counter - 1))" "iterations_$((iter)).sqrt")
        sqrt11=$(yq r ./results_lab2.yaml -d "$((counter - 1))" "iterations_$((iter)).sqrt1_onetime")
        sqrt14=$(yq r ./results_lab2.yaml -d "$((counter - 1))" "iterations_$((iter)).sqrt1_fourtimes")
        sqrt2=$(yq r ./results_lab2.yaml -d "$((counter - 1))" "iterations_$((iter)).sqrt2")
        sqrt3=$(yq r ./results_lab2.yaml -d "$((counter - 1))" "iterations_$((iter)).sqrt3")
        
        sqrt_avg=$(python -c "print $sqrt_avg + $sqrt")
        sqrt11_avg=$(python -c "print $sqrt11_avg + $sqrt11")
        sqrt14_avg=$(python -c "print $sqrt14_avg + $sqrt14")
        sqrt2_avg=$(python -c "print $sqrt2_avg + $sqrt2")
        sqrt3_avg=$(python -c "print $sqrt3_avg + $sqrt3")
        
        ((counter++))
    done

    echo $(python -c "print $sqrt_avg / 10")
    echo $(python -c "print $sqrt11_avg / 10")
    echo $(python -c "print $sqrt14_avg / 10")
    echo $(python -c "print $sqrt2_avg / 10")
    echo $(python -c "print $sqrt3_avg / 10")
}

runTests
exit 0