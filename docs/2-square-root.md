# Übungsblatt 2: Quadratwurzel

## Teilaufgabe 1

In Teilaufgabe 1 war die simple Umsetzung des Newtonverfahrens gefragt, die mit
dem Startwert aus dem vorgegebenen Code ausgeführt werden soll. (Und zwar so
oft, wie in der Konstante `LOOPS` vorgegeben.) Der Quelltext für diese Funktion
sieht wie folgt aus:

```cpp
float sqrt1(float * a) {

  float root;

  int *ai = reinterpret_cast<int *>(a);
  int *initial = reinterpret_cast<int *>(&root);
  *initial = (1 << 29) + (*ai >> 1) - (1 << 22) - 0x4C000;
  root = *reinterpret_cast<float *>(initial);

  for (unsigned int i = 0; i < LOOPS; i++) {
    root = 0.5 * (root + *a / root);
  }
  
  return root;
}
```

Die geforderte Lösung umfasst das zurückcasten des `*initial`-Werts zum Datentyp
`float`, der dann als Startwert in die Berechnung der Nullstelle mit dem
Newton-Algorithmus eingeht. Die eigentliche Umsetzung des Verfahrens selbst ist
simpel und umfasst die programmatische Realisierung des auf dem Übungsblatt
vorgegebenen mathematischen Vorgehens, wobei die Variable `root` am Anfang den
Startwert x<sub>0</sub> enthält und sich mit jeder Iteration `sqrt(a)` annähert.

## Teilaufgabe 2

## Teilaufgabe 3

## Teilaufgabe 4
