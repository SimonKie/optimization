# Übungsblatt 1: Intersection

## Vorbereitung

Im Vorfeld der Bearbeitung der Übungsblätter wurde der Output des Raytracers und
des Statistik-Helfers geringfügig angepasst, sodass die Aufgabe im
YAML-Format erfolgt:

```yaml
parameters:
  reading: "examples/teapot.obj"
  writing: "output.bmp"
  x_resolution: 256
  y_resolution: 256
  reverse_vertices: 0
  write_ppm: 0
wavefront:
  triangles: 6320
  vertices: 3644
  normals: 3644
  memory_used: 455040
statistics:
  float: "float"
  size_of_float: 4
  time: 9.16276
  intersection_tests: 519950720
  intersections_found: 38215
```

Dadurch kann der Output einfacher maschinell weiterverarbeitet werden, z. B.
durch das Skript `test.sh`. Dieses führt automatisiert eine vorgegebene Anzahl
an Durchläufen aus und berechnet die Durchschnittszeit (vollständiges Skript:
s. u.).

## Benchmark

Der unangepasste Code liefert folgende Zeiten (Ausgabe des Test-Skripts):

```txt
Iteration 1: 7.21068 seconds
Iteration 2: 7.40499 seconds
Iteration 3: 7.2584 seconds
Iteration 4: 7.59909 seconds
Iteration 5: 7.43201 seconds
Iteration 6: 7.82095 seconds
Iteration 7: 7.49702 seconds
Iteration 8: 7.59119 seconds
Iteration 9: 7.29561 seconds
Iteration 10: 7.0752 seconds
Average time for 10 iterations: 7.418514 seconds
```

## Neu-Implementierung der `intersect`-Methode

Wie in der Aufgabenstellung gefordert wurden an der bereits implementierten
Funktion drei Punkte geändert:

* Eine zusätzliche Überprüfung, ob `t` größer ist als der bisherige
  Schnittpunktparameter `minimum_t`:

  ```cpp
  // if t is greater than the intersection parameter minimum_t, return early
    if (t < 0.0 || t > minimum_t) {
      return false;
    }
  ```

* Die Verschiebung der Berechnung der baryzentrischen Koordinaten `u` und `v`
  ans Ende der Methode, nachdem alle Abbruchkrieterien überprüft wurden,
* Sowie die Kalkulation dieser Größen über die Hilfsmethode `square_of_length()`
  statt des dreimaligen Berechnens der Quadratwurzel mit der Methode `length()`:

  ```cpp
  vector = cross_product(p3 - p2,  intersection - p2 );
  if ( normal.scalar_product(vector) < 0.0 ) {
    return false;
  }
  
  T vSquareOfLength = vector.square_of_length();
  
  vector = cross_product(p1 - p3, intersection - p3 );
  if (normal.scalar_product(vector) < 0.0 ) {
    return false;
  }

  T uSquareOfLength = vector.square_of_length();
  
  T areaSquareOfLength = normal.square_of_length();
  u = sqrt(uSquareOfLength / areaSquareOfLength);
  v = sqrt(vSquareOfLength / areaSquareOfLength);
  ```

Die Neu-Implementierung erzielt im Test mit dem angepassten Testskript eine
leichte Verbesserung bezüglich der Laufzeit. Die Ergebnisse:

```txt
Iteration 1: 6.29286 seconds
Iteration 2: 6.3274 seconds
Iteration 3: 6.18682 seconds
Iteration 4: 6.27748 seconds
Iteration 5: 6.52067 seconds
Iteration 6: 7.08268 seconds
Iteration 7: 7.06333 seconds
Iteration 8: 7.09651 seconds
Iteration 9: 6.93197 seconds
Iteration 10: 7.10875 seconds
Average time for 10 iterations: 6.688847 seconds
```

Im Vergleich mit den 7.418514 Sekunden im ersten Durchlauf ergibt sich eine
Verbesserung um ca. 0.73 Sekunden oder 9,84%.

## Untersuchung mit `gcov`

Ursprüngliche Variante:

```txt
519950720:   43:  bool intersects(Vector<T,3> origin, Vector<T,3> direction,
        -:   44:                   FLOAT &t, FLOAT &u, FLOAT &v, FLOAT minimum_t = INFINITY) {
519950720:   45:    Vector<T, 3> normal =  cross_product(p2 - p1, p3  - p1);
        -:   46:    
        -:   47:    T normalRayProduct = normal.scalar_product( direction );
        -:   48:    T area = normal.length(); // used for u-v-parameter calculation
        -:   49:
519950720:   50:    if ( fabs(normalRayProduct) < EPSILON ) {
        -:   51:      return false;
        -:   52:    }
        -:   53:
        -:   54:    T d = normal.scalar_product( p1 );
519950711:   55:    t = (d - normal.scalar_product( origin ) ) / normalRayProduct;
        -:   56:
519950711:   57:    if ( t < 0.0 ) {
        -:   58:      return false;
        -:   59:    }
        -:   60:   
458857871:   61:    Vector<T, 3> intersection = origin + t * direction;
        -:   62:   
458857871:   63:    Vector<T, 3> vector = cross_product(p2 - p1,  intersection - p1 );
458857871:   64:    if ( normal.scalar_product(vector) < 0.0 ) { 
        -:   65:      return false;
        -:   66:    }
        -:   67:
        -:   68:    
233495364:   69:    vector = cross_product(p3 - p2,  intersection - p2 );
233495364:   70:    if ( normal.scalar_product(vector) < 0.0 ) { 
        -:   71:      return false;
        -:   72:    }
        -:   73:
116113733:   74:    u = vector.length()  / area;
        -:   75:
        -:   76:
116113733:   77:    vector = cross_product(p1 - p3, intersection - p3 );
116113733:   78:    if (normal.scalar_product(vector) < 0.0 ) {
        -:   79:      return false;
        -:   80:    }
        -:   81:
    38215:   82:    v = vector.length() / area;
        -:   83:
    38215:   84:    return true;
        -:   85:  }
```

Editierte Variante:

```txt
519950720:   89:bool intersects(Vector<T,3> origin, Vector<T,3> direction,
        -:   90:                   FLOAT &t, FLOAT &u, FLOAT &v, FLOAT minimum_t) {
519950720:   91:    Vector<T, 3> normal =  cross_product(p2 - p1, p3  - p1);
        -:   92:    
        -:   93:    T normalRayProduct = normal.scalar_product( direction );
        -:   94:
519950720:   95:    if ( fabs(normalRayProduct) < EPSILON ) {
        -:   96:      return false;
        -:   97:    }
        -:   98:
        -:   99:    T d = normal.scalar_product( p1 );
519950711:  100:    t = (d - normal.scalar_product( origin ) ) / normalRayProduct;
        -:  101:   
        -:  102:    // if t is greater than the intersection parameter minimum_t, return early 
519950711:  103:    if (t < 0.0 || t > minimum_t) {
        -:  104:      return false;
        -:  105:    }
        -:  106:
413970453:  107:    Vector<T, 3> intersection = origin + t * direction;
        -:  108:   
413970453:  109:    Vector<T, 3> vector = cross_product(p2 - p1,  intersection - p1 );
413970453:  110:    if ( normal.scalar_product(vector) < 0.0 ) { 
        -:  111:      return false;
        -:  112:    }
        -:  113:
210496532:  114:    vector = cross_product(p3 - p2,  intersection - p2 );
210496532:  115:    if ( normal.scalar_product(vector) < 0.0 ) { 
        -:  116:      return false;
        -:  117:    }
        -:  118:
        -:  119:    T uSquareOfLength = vector.square_of_length();
        -:  120:
104877228:  121:    vector = cross_product(p1 - p3, intersection - p3 );
104877228:  122:    if (normal.scalar_product(vector) < 0.0 ) {
        -:  123:      return false;
        -:  124:    }
        -:  125:
        -:  126:    T vSquareOfLength = vector.square_of_length();
        -:  127:
        -:  128:    T areaSquareOfLength = normal.square_of_length();
    35294:  129:    u = sqrt(uSquareOfLength / areaSquareOfLength);
    35294:  130:    v = sqrt(vSquareOfLength / areaSquareOfLength);
        -:  131:
    35294:  132:    return true;
        -:  133:  }
```

Folgende Aussagen können getroffen werden:

* Die Anzahl der Methodenaufrufe ist in beiden Fällen gleich - alles andere wäre
  Grund zur Sorge. Genaue Anzahl: 519.950.720
* Der Vergleich von `t` mit `minimum_t` beendet 44.887.418 Aufrufe frühzeitig:
  413.970.453 statt 458.857.871 mal wird die darauffolgende Zeile aufgerufen.
* Die Anzahl der Aufrufe der Qudratwurzel-Längenberechnungen konnten vor allem
  für die Größe `u` signifikant reduziert werden.

## Untersuchung mit `gprof`

Ursprüngliche Variante:

```txt
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
100.02      6.99     6.99                             raytrace(Camera&, Scene&, Screen&, KDTree*)
  0.00      6.99     0.00       14     0.00     0.00  void std::vector<Triangle<float>, std::allocator<Triangle<float> > >::_M_realloc_insert<Triangle<float> const&>(__gnu_cxx::__normal_iterator<Triangle<float>*, std::vector<Triangle<float>, std::allocator<Triangle<float> > > >, Triangle<float> const&)
  0.00      6.99     0.00       13     0.00     0.00  void std::vector<Vector<float, 3ul>, std::allocator<Vector<float, 3ul> > >::_M_realloc_insert<Vector<float, 3ul> const&>(__gnu_cxx::__normal_iterator<Vector<float, 3ul>*, std::vector<Vector<float, 3ul>, std::allocator<Vector<float, 3ul> > > >, Vector<float, 3ul> const&)
  0.00      6.99     0.00        1     0.00     0.00  _GLOBAL__sub_I__ZN10Statistics5printEv
  0.00      6.99     0.00        1     0.00     0.00  _GLOBAL__sub_I__ZlsRSoRK6Screen
```

Editierte Variante:

```txt
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
100.02      6.12     6.12                             raytrace(Camera&, Scene&, Screen&, KDTree*)
  0.00      6.12     0.00       14     0.00     0.00  void std::vector<Triangle<float>, std::allocator<Triangle<float> > >::_M_realloc_insert<Triangle<float> const&>(__gnu_cxx::__normal_iterator<Triangle<float>*, std::vector<Triangle<float>, std::allocator<Triangle<float> > > >, Triangle<float> const&)
  0.00      6.12     0.00       13     0.00     0.00  void std::vector<Vector<float, 3ul>, std::allocator<Vector<float, 3ul> > >::_M_realloc_insert<Vector<float, 3ul> const&>(__gnu_cxx::__normal_iterator<Vector<float, 3ul>*, std::vector<Vector<float, 3ul>, std::allocator<Vector<float, 3ul> > > >, Vector<float, 3ul> const&)
  0.00      6.12     0.00        1     0.00     0.00  _GLOBAL__sub_I__ZN10Statistics5printEv
  0.00      6.12     0.00        1     0.00     0.00  _GLOBAL__sub_I__ZlsRSoRK6Screen
```

Die Analyse deckt sich ungefähr mit den oben berechneten Einsparungen.

## Das verwendete Test-Skript

Das Test-Skript kann mit einer beliebigen Anzahl an Iterationen aufgerufen
werden und führt die angebene Anzahl an Programmaufrufen aus, nachdem der
Quellcode einmalig mit einem vorgegebenen Befehl compiled wurde. Die
YAML-Ausgabe wird in ein Logfile geschrieben, währenddessen behält das Skript
die Durchlaufzeiten und kalkuliert am Ende die Durchschnittszeit. Es benötigt
den [yq](https://github.com/mikefarah/yq) YAML Command Line Processor sowie
Python für die Berechnung des Durchschnitts.

```bash
#!/bin/bash

ITERATIONS=10
COMPILE=true
COMPILE_COMMAND="g++ -Wall -pedantic -march=native -mfpmath=sse -mavx -O3 raytracer.cc statistics.cc"

function usage()
{
    echo "Test the raytracer repeatedly."
    echo ""
    echo "./test.sh"
    echo "    -h --help"
    echo "    -i --iterations=$ITERATIONS"
    echo ""
}

function compile()
{
    if [ $COMPILE = true ]
    then
        echo "Executing compile command:"
        echo $COMPILE_COMMAND
        eval $COMPILE_COMMAND
        echo "Done compiling"
    fi
}

function runTests() {
    counter=1
    fulltime=0

    # empty the logfile before execution
    echo "---" > ./../log.yaml

    while [ $counter -le $ITERATIONS ]
    do
        # redirect the raytracers error output to standard out,
        # then pipe it through yq (YAML handler) into a log file.
        ./a.out --no_ppm 2>&1 | yq r - >> ./../log.yaml
        echo "---" >> ./../log.yaml

        # Use yq to get the time for the run
        time=$(yq r ./../log.yaml -d "$((counter - 1))" "statistics.time")
        echo "Iteration $counter: $time seconds"

        # Add the increment to the overall time
        fulltime=$(python -c "print $fulltime + $time")

        ((counter++))
    done
}

function displayResults()
{
    # Calculate and display average time
    avgtime=$(python -c "print $fulltime / $ITERATIONS")
    echo "Average time for $ITERATIONS iterations: $avgtime seconds"
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        -i | --iterations)
            ITERATIONS=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

cd src
compile
runTests
displayResults
exit 0
```
